#!/usr/bin/env python

# # General notebook for calculations 

import configparser
import os
import pandas as pd
import sys

# cells for functions from area and production numbers
def total_stubble_prod(production, harvest_index):
    '''Total Stubble Production 'As Is' MC [t] = ABS Prod[t]*(1-0.36)/0.36 = A'''
    result = production*(1-harvest_index)/harvest_index
    return result

def total_subble_yield(area, production, harvest_index):
    '''Total Stubble Yield 'As Is' MC [t/ha] = A/ABS Area[ha] = B'''
    tsp = total_stubble_prod(production, harvest_index)
    result = tsp/area
    return result

# TODO: 82% needs to be a config param but I cant find the data
# why not 1-mc?
#def straw_yield(area, production, harvest_index, retain, mc):
def straw_yield(area, production, harvest_index, retain):
    '''Straw Yield 'As Is' MC [t/ha] = (B*(82/100))-1.0 = C'''
    tsy = total_subble_yield(area, production, harvest_index)
    result = (tsy*(82.0/100.0)) - 1
    return result

def straw_prod(area, production, harvest_index, retain):
    '''Straw Production 'As Is' MC [t] = C*ABS Area[ha] = D'''
    sy = straw_yield(area, production, harvest_index, retain)
    result = sy * area
    return result

def straw_yield_adj(area, production, harvest_index, retain):
    '''Adjusted Straw Yield 'As Is'MC [t/ha] = IF(C<0,0,C)'''
    sy = straw_yield(area, production, harvest_index, retain)
    if sy < 0:
        return 0
    else:
        return sy

def straw_prod_adj(area, production, harvest_index, retain):
    '''Adjusted Straw Production 'As Is' MC [t] = IF(D<0,0,D) = E'''
    sp = straw_prod(area, production, harvest_index, retain)
    if sp < 0:
        return 0
    else:
        return sp

def percent_straw_prod(area, production, harvest_index, retain, mc):
    '''Straw Production 0% MC [t] = E*0.88 = F'''
    spa = straw_prod_adj(area, production, harvest_index, retain)
    result = spa * (1-mc) #0.88
    return result

# get area and production for a given year
def get_area_prod(df, year='2013_14', commod='wheat',
                  region='new_south_wales'):
    try:
        df['commodity']==commod
        data = df[df['capture_year']==year][df['commodity']==commod][df['region_label']==region]
        units = list(data['unit'])
        area = pd.to_numeric(data[data['unit']==units[0]]['estimate'], errors='coerce')
        production = pd.to_numeric(data[data['unit']==units[1]]['estimate'], errors='coerce')
        return (area.values[0], production.values[0], units[:2])
    except IndexError:
        # no data for this crop in this region
        return (None, None, None)

# functions for 5 yearly calc
def ave_straw_years(df, years, harvest_index, retain):
    '''AVG Straw 'As Is' MC [t] = AVERAGE(E) - of 5 years'''
    estimates = []
    for year in years:
        area, production, units = get_area_prod(df=df, year=year)
        result = straw_prod_adj(area, production, harvest_index, retain)
        estimates.append(result)
    ave =  sum(estimates)/len(estimates)
    return ave

def min_straw_years(df, years, harvest_index, retain):
    '''MIN Straw 'As Is' MC [t] = MIN(E) - of 5 years'''
    estimates = []
    for year in years:
        area, production, units = get_area_prod(df=df, year=year)
        result = straw_prod_adj(area, production, harvest_index, retain)
        estimates.append(result)
    mins = min(estimates)
    return mins

def max_straw_years(df, years, harvest_index, retain):
    '''MAX Straw 'As Is' MC [t] = MAX(E) - of 5 years'''
    estimates = []
    for year in years:
        area, production, units = get_area_prod(df=df, year=year)
        result = straw_prod_adj(area, production, harvest_index, retain)
        estimates.append(result)
    maxs = max(estimates)
    return maxs

def ave_straw_years_percent(df, years, harvest_index, retain, mc):
    '''AVG Straw 0% MC [t] = AVERAGE(F) - of 5 years'''
    estimates = []
    for year in years:
        area, production, units = get_area_prod(df=df, year=year)
        result = percent_straw_prod(area, production, harvest_index, retain, mc)
        estimates.append(result)
    ave = sum(estimates)/len(estimates)
    return ave

def min_straw_years_percent(df, years, harvest_index, retain, mc):
    '''MIN Straw 0% MC [t] = MIN(F) - of 5 years'''
    estimates = []
    for year in years:
        area, production, units = get_area_prod(df=df, year=year)
        result = percent_straw_prod(area, production, harvest_index, retain, mc)
        estimates.append(result)
    mins = min(estimates)
    return mins

def max_straw_years_percent(df, years, harvest_index, retain, mc):
    '''MAX Straw 0% MC [t] = MAX(F) - of 5 years'''
    estimates = []
    for year in years:
        area, production, units = get_area_prod(df=df, year=year)
        result = percent_straw_prod(area, production, harvest_index, retain, mc) 
        estimates.append(result)
    maxs = max(estimates)
    return maxs


## beginning of main script ##
# os.path.curdir()
if __name__ == '__main__':
    # use input
    try:
        infile_csv = sys.argv[1]
        df = pd.read_csv(infile_csv)
        print('=====Target=====')
        print(df.head())
    except:
        # catch everything for now
        print('Usage: python calculations.py <INFILE.CSV>')
        print('Is the file missing or not a CSV?')
        print('Gracefully quiting now..')
        sys.exit(1)

    # clean the df 1) cols 2) vals
    df.columns = df.columns.str.strip().str.replace(' ', '_').str.replace('-','_').str.lower()
    df = pd.concat([df[col].astype(str).str.strip().str.replace(' ', '_').str.replace('-','_').str.lower()
                    for col in df.columns], axis=1)
    print('====Target after cleaning====')
    print(df.head())
    # config file reading
    config = configparser.ConfigParser()
    config.read('config.ini')
    # now we try with the access function
    # this should be from a config file or something?
    # hard code it for now for testing
    # lowe case
    year = '2017_18'
    for region in config['RETAIN']:
        for commod in config['HI']:
            print(region)
            print(commod)
            print('commod = %s' %commod)
            harvest_index = float(config['HI'][commod])
            retain = float(config['RETAIN'][region])
            mc = float(config['MC'][commod]) / 100.00
            area, production, units = get_area_prod(df=df, year=year, commod=commod,
                                                    region=region)
            # test if not None as in crop not grown in area/year
            if production:
                print('%s area: %s' % (year, area))
                print('%s production: %s' % (year, production))
                tsp = total_stubble_prod(production, harvest_index)
                print('total_stubble_prod = %s' %  tsp)
                tsy = total_subble_yield(area, production, harvest_index)
                print('total_subble_yield = %s' % tsy)
                sy = straw_yield(area, production, harvest_index, retain)
                print('straw_yield = %s' % sy)
                sp = straw_prod(area, production, harvest_index, retain)
                print('straw_prod = %s' % sp)
                sya = straw_yield_adj(area, production, harvest_index, retain)
                print('straw_yield_adj = %s' % sya)
                spa = straw_prod_adj(area, production, harvest_index, retain)
                print('straw_prod_adj = %s' % spa)
                psp = percent_straw_prod(area, production, harvest_index, retain, mc)
                print('percent_straw_prod = %s' % psp)

                # Five year data, this needs to come from a config file or cmd line
                years = ['2010_11', '2014_15', '2017_18', '2012_13', '2016_17', 
                        '2011_12', '2015_16', '2013_14']
                years = sorted(years, key=lambda x: int(x.split('_')[1]))
                years = years[-5:]

                print('ave_straw_years')
                print(ave_straw_years(df, years, harvest_index, retain))
                print('min_straw_years')
                print(min_straw_years(df, years, harvest_index, retain))
                print('max_straw_years')
                print(max_straw_years(df, years, harvest_index, retain))
                print('ave_straw_years_percent')
                print(ave_straw_years_percent(df, years, harvest_index, retain, mc))
                print('min_straw_years_percent')
                print(min_straw_years_percent(df, years, harvest_index, retain, mc))
                print('max_straw_years_percent')
                print(max_straw_years_percent(df, years, harvest_index, retain, mc))
