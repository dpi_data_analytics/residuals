#!/usr/bin/env python

import argparse
import configparser
import csv
import math
import numpy as np
import sys

# external
import pandas as pd
import xlsxwriter

# helper to create years
def create_year_list(year_start, year_end):
    years = [year_start]
    start = int(year_start.split('_')[1])
    end = start + 4 # want 5 years but first year in list already
    for y in range(start, end):
        years.append('20%s_%s'%(y, y+1))
    return years

# format numbers with commas
def comma_format(number):
    if number == '':
        # empty value
        return number
    else:
        formatted = "{number:,}".format(number=number)
        return formatted

# main calc function
def make_residue_calcs(area, production, harvest_index,
                       retain, mc, harvestable):
    # Total Stubble Productn 'As Is' MC [t] = ABS Prod[t]*(1-0.36)/0.36 = A
    if harvest_index != 0:
        tsp = production * (1 - harvest_index) / harvest_index
    else:
        tsp = production
    # Total Stubble Yield 'As Is' MC [t/ha] = A/ABS Area[ha] = B
    tsy = tsp / area
    # Straw Yield 'As Is' MC [t/ha] = (B*(82/100))-1.0 = C
    sy = (tsy * harvestable) - retain
    # Straw Production 'As Is' MC [t] = C*ABS Area[ha] = D
    sp = sy * area
    # Straw Yield adjust 'As Is'MC [t/ha] = IF(C<0,0,C)
    if sy < 0:
        sya = 0
    else:
        # numbers are immutable so this is not a reference pointer
        sya = sy
    # Straw Production Adj 'As Is' MC [t] = IF(D<0,0,D) = E
    if sp < 0:
        spa = 0
    else:
        spa = sp
    # percent Straw Production 0% MC [t] = E*0.88 = F
    psp = spa * (1-mc) #0.88
    return (tsp, tsy, sy, sp, sya, spa, psp)


# get area and production for a given year
def get_area_prod(df, year, commod, region):
    try:
        df['commodity']==commod
        data = df[df['capture_year']==year][df['commodity']==
                                            commod][df['region_label']==region]
        units = list(data['unit'])
        area = pd.to_numeric(data[data['unit']==
                                  units[0]]['estimate'], errors='coerce')
        production = pd.to_numeric(data[data['unit']==
                                        units[1]]['estimate'],
                                   errors='raise')
        # np values in spreadsheet mean no data
        if math.isnan(area.values[0]) or math.isnan(production.values[0]):
            return (0, 0, units[:2])
        else:
            return (area.values[0], production.values[0], units[:2])
    except IndexError:
        # no data for this crop in this region
        return (None, None, None)

def write_xlsx(wsheet, row, data):
    '''write a csv row to excel'''
    col = 0
    for cell in data:
        wsheet.write(row, col, cell)
        col += 1

# command line parser
my_parser = argparse.ArgumentParser(description='Report residue data')
my_parser.add_argument('-i', '--infile', action='store', type=str,
                       required=True, help='Infile CSV/Excel for parsing',
                       dest='infile_csv')

my_parser.add_argument('-o', '--outfile', action='store', type=str,
                       required=True, help='Outfile CSV for writing',
                       dest='outfile_csv')

my_parser.add_argument('-c', '--config', action='store', type=str,
                       required=True, help='Config file',
                       dest='config')

my_parser.add_argument('-x', '--xlsx', action='store', type=str,
                       required=True, help='Outfile XLSX for writing',
                       dest='outfile_xlsx')


## beginning of main script ##
# os.path.curdir()
if __name__ == '__main__':
    # this eventully come via the ABoS API....
    args = my_parser.parse_args()

    # Write a excel file
    # https://xlsxwriter.readthedocs.io/tutorial01.html
    workbook = xlsxwriter.Workbook(args.outfile_xlsx)
    worksheet = workbook.add_worksheet()

    if args.infile_csv.find('csv') != -1:
        df = pd.read_csv(args.infile_csv)
    else:
        try:
            df = pd.read_excel(args.infile_csv)
        except:
            print("Can't parse infile, is it CSV or Excel?")
            sys.exit(1)

    df.rename(columns=lambda x: x.strip(), inplace=True)
    df['Estimate'] = (df['Estimate'].replace(',','', regex=True)
                      .replace('np', 0, regex=True)
                      .astype(float))

    print('=====Target=====')
    print(df.head())

    # fix name issue with newcastle
    df.replace('Hunter Valley excluding Newcastle',
               'Hunter Valley exc Newcastle', inplace=True)
    # fix any use of - instead of 0
    df.replace({'Estimate': '-'}, 0.0, inplace=True)
    # clean the df 1) cols 2) vals
    df.columns = df.columns.str.strip().str.replace(' ', '_').str.replace('-','_').str.lower()
    df = pd.concat([df[col].astype(str).str.strip().str.replace(' ', '_').str.replace('-','_').str.lower()
                    for col in df.columns], axis=1)
    # config file reading
    config = configparser.ConfigParser()
    config.read(args.config)
    # TODO: These 3 lines should not be hard coded use config?
    year_start = '2013_14'
    year_end = '2017_18'
    outfile = open(args.outfile_csv, 'w')
    csv_writer = csv.writer(outfile)
    header = ['region',
              'commodity',
              'year',
              'HI',
              'MC',
              'harvestable',
              'retention',
              'ABS_unit',
              'ABS_production',
              'ABS_area',
              'stubble_prod_harv',
              'stubble_yield_harv',
              'straw_yield_harv',
              'straw_prod_harv',
              'straw_yield_harv_adj',
              'straw_prod_harv_adj',
              'straw_prod_zero',
              'ave_straw_harv',
              'min_straw_harv',
              'max_straw_harv',
              'ave_straw_zero',
              'min_straw_zero',
              'max_straw_zero',
              ]
    row = 0
    csv_writer.writerow(header)
    write_xlsx(worksheet, row, header)
    row += 1
    regions = list(config['RETAIN'])
    # exclude list
    exclude_list = ['new_south_wales', 'nt', 'qld',
                    'nswnorth','nswsouth','vic','sa',
                    'tas','wa']
    for exclude in exclude_list:
        regions.remove(exclude)
    # sort alp by region
    regions.sort()
    commods_dict = {}
    # region calcs
    for region in regions:
        print('Processing data from %s' % region)
        for commod in config['HI']:
            # create a list to hold the row data for 5 year info
            production_data = []
            spa_data = []
            psp_data = []
            #production_data.append(commod)
            harvest_index = float(config['HI'][commod])
            harvestable = float(config['GENERAL']['harvestable'])
            retain = float(config['RETAIN'][region])
            mc = float(config['MC'][commod]) / 100.00
            years = create_year_list(year_start, year_end)
            # check if any of the years have values, if so use 0
            check_data = [get_area_prod(df, year, commod, region)[1]
                          for year in years]
            data_flag = False
            for d in check_data:
                if isinstance(d, np.float64) or isinstance(d, np.int64):
                    # skip 0 values
                    if d == 0.0:
                        pass
                    else:
                        data_flag = True
            for year in years:
                year_data = []
                year_data.append(region)
                year_data.append(commod)
                year_data.append(year.replace('_','-'))
                year_data.append(harvest_index)
                year_data.append(mc)
                year_data.append(harvestable)
                year_data.append(retain)
                area, production, units = get_area_prod(df, year,
                                                        commod, region)

                # if there is no data for this year skip
                if production:
                    if units[1].find('kg') != -1:
                        # convert kg to t
                        production = float(production/1000.0)
                        units[1] = 'production_(t)'

                    year_data.append(units[1].replace('_',' '))
                    year_data.append(comma_format(round(production)))
                    year_data.append(comma_format(round(area)))
                    # returns (tsp, tsy, sy, sp, sya, spa, psp)
                    residue_data = make_residue_calcs(area, production,
                                                      harvest_index,
                                                      retain, mc,
                                                      harvestable)

                    # tsp
                    year_data.append(comma_format(round(residue_data[0]))) # not rounded
                    # tsy
                    year_data.append(comma_format(round(residue_data[1], 2))) # not rounded
                    # sy
                    year_data.append(comma_format(round(residue_data[2], 2)))
                    # sp
                    year_data.append(comma_format(round(residue_data[3])))
                    # sya
                    year_data.append(comma_format(round(residue_data[4], 2)))
                    # spa
                    year_data.append(comma_format(round(residue_data[5])))
                    # add this to the 5 year list
                    spa_data.append(residue_data[5])
                    # psp
                    year_data.append(comma_format(round(residue_data[6])))
                    # add this to the 5 year list
                    psp_data.append(residue_data[6]) # do not round
                else:
                    if data_flag:
                        # there is data for one year at least, so make vals 0
                        #spa, psp = 0, 0
                        year_data+=['','','', '', '', '', '', '', 0, 0]
                        spa_data.append(0)
                        psp_data.append(0)

                # end of year data write row
                if data_flag:
                    csv_writer.writerow(year_data)
                    write_xlsx(worksheet, row, year_data)
                    row += 1

            # end of commod write out averages
            five_yr_range = '%s-%s' % (year_start.split('_')[0],
                                       year_end.split('_')[1])
            if units:
                unit = 'production (t)'
            else:
                unit = ''
            year_5_info = [region, commod, five_yr_range,
                           harvest_index, mc, harvestable, retain,
                           unit,'','','','','','','','','']
            if len(spa_data) > 0:
                asya = round(sum(spa_data) / 5) # NOTE: divide by 5, could div by lengt
                msya = round(min(spa_data))
                mxsya = round(max(spa_data))
                totals = [asya]
            else:
                asya, msya, mxsya = '','',''
                totals = [0.0]
            if len(psp_data) > 0:
                apsp = round(sum(psp_data) / 5)
                mpsp = round(min(psp_data))
                mxpsp = round(max(psp_data))
                totals.append(apsp)
            else:
                apsp, mpsp, mxpsp = '','',''
                totals.append(0.0)
            # capture the totals for this commod
            if commod in commods_dict:
                commods_dict[commod].append(totals)
            else:
                commods_dict[commod] = [totals]
            year_5_info+=[comma_format(asya), comma_format(msya),
                          comma_format(mxsya), comma_format(apsp),
                          comma_format(mpsp), comma_format(mxpsp)]
            # only write data if something to show in at least 1 of 5 yrs
            if data_flag:
                csv_writer.writerow(year_5_info)
                write_xlsx(worksheet, row, year_5_info)
                row += 1

    # calculate the totals based on the last 6 cols
    for commod in commods_dict:
        total_info = ['Totals', commod, five_yr_range,
                      '', '', '', '',
                      'production (t)','','','','','','','','','']
        nsw_totals_sums = list(map(sum, zip(*commods_dict[commod])))
        total_info += [comma_format(nsw_totals_sums[0]), '','',
                        comma_format(nsw_totals_sums[1]), '', '']
        csv_writer.writerow(total_info)
        write_xlsx(worksheet, row, total_info)
        row += 1
    outfile.close()
    #workbook.close()
    # finally just make a second sheet
    # with the calcs
    worksheet2 = workbook.add_worksheet('formulia')
    # row, col, data
    # area
    worksheet2.write(0, 6, 'Wheat calcs')
    worksheet2.write(1, 6, 'Area (ha)')
    worksheet2.write(1, 7, 3212411.99)
    worksheet2.write(2, 6, 'Production (t)')
    worksheet2.write(2, 7, 6506239.87)
    worksheet2.write(6, 0, 'Total Stubble Production As Is MC [t] = ABS Prod[t]*(1-0.36)/0.36 = A')
    worksheet2.write(6, 1, 'Total Stubble Yield As Is MC [t/ha] = A/ABS Area[ha] = B')
    worksheet2.write(6, 2, 'Straw Yield As Is MC [t/ha] = (B*(82/100))-1.0 = C')
    worksheet2.write(6, 3, 'Straw Production As Is MC [t] = C*ABS Area[ha] = D')
    worksheet2.write(6, 4, 'Adjusted Straw Yield As Is MC [t/ha] = IF(C<0,0,C)')
    worksheet2.write(6, 5, 'Adjusted Straw Production As Is MC [t] = IF(D<0,0,D) = E')
    worksheet2.write(6, 6, 'Straw Production 0% MC [t] = E*0.88 = F')
    worksheet2.write(6, 7, 'AVG Straw As Is MC [t] = AVERAGE(E) - of 5 years')
    worksheet2.write(6, 8, 'MIN Straw As Is MC [t] = MIN(E) - of 5 years')
    worksheet2.write(6, 9, 'MAX Straw As Is MC [t] = MAX(E) - of 5 years')
    worksheet2.write(6, 10, 'AVG Straw 0% MC [t] = AVERAGE(F) - of 5 years')
    worksheet2.write(6, 11, 'MIN Straw 0% MC [t] = MIN(F) - of 5 years')
    worksheet2.write(6, 12, 'MAX Straw 0% MC [t] = MAX(F) - of 5 years')

    # calcs
    worksheet2.write(7, 0, '=H3*(1-0.36)/0.36')
    worksheet2.write(7, 1, '=A8/H2')
    worksheet2.write(7, 2, '=(B8*(82/100))-1')
    worksheet2.write(7, 3, '=C8*H2')
    worksheet2.write(7, 4, '=IF(C8<0,0,C8)')
    worksheet2.write(7, 5, '=IF(D8<0,0,D8)')
    worksheet2.write(7, 6, '=F8*0.88')

    # cleanup
    workbook.close()
