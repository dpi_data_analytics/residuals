import xlsxwriter

workbook = xlsxwriter.Workbook('cal_test.xlsx')
worksheet = workbook.add_worksheet()
'''
expenses = (['rent', 1000],
            ['Gas', 100],
            ['trees', 200],
            ['stamps', 10],
            )

row = 0
col = 0

for item, cost in (expenses):
    worksheet.write(row, col, item)
    worksheet.write(row, col + 1, cost)
    row += 1

worksheet.write(row, 0, 'Total')
worksheet.write(row, 1, '=SUM(B1:B4)')
'''

# area (HA)
worksheet.write('H2',3212411.99)
# prod (t)
worksheet.write('H3', 6506239.87)

worksheet.write('A8', '=H3*(1-0.36)/0.36')
workbook.close()
