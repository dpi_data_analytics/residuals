# 202005CCARNEY: Automation of ABS/ABARES residual data processing  

## Project goal  
Provide a scripting framework for summary calculations based on ABS/ABARES residue data. The key information processed by the software is the total area of holding in hectares for a given crop and the resulting production total of the crop in question. The output contains various residue calculations for a given year, as well as the averages based on the previous 5 years.  

A config file allows the user to set specific values for 'Retain', 'harvest index (HI)', and 'moisture content (MC)'(check??) across regions and crops. Input data from any regions or crops not listed in the config file will be ignored.  

Example input data and the subsequent processed output data can be found in the `example_data` directory. Config files for SA and NSW can also be found in the `configs` directory. The best way to get these scripts to work is to follow the formatting used in these files, however moving forward I would recommend using only lower case letters, and replacing spaces with underscores, and this will make your output more reliable.   

## Quick start  
Assumes you have the free version of Anaconda python installed. This provides the python environment for running the scrips and installing the required python libraries. Instructions for installing Anaconda can be found at https://www.anaconda.com/products/individual.  

### 1. Download the software repository  

Navigate to `https://bitbucket.org/dpi_data_analytics/residuals/src/master/`, click the 3 dots on the left hand side next to the 'Clone' button and click 'Download repository'. Unzip the file to a known location and open a console/shell window inside the main root directory of the software repository.  

### 2. Install the conda environment based on the supplied residue_env.yml file.  
From inside the main root directory of the repository type in the terminal or command line window.  

```
conda env create -f env/residue_env.yml
```

This should install several python libraries required by the software, these will be installed in the users 'work space' and will not impact other users.  

### 3. Activate the environment   

```
conda activate residue
```
Ensure that the command prompt changes to should show '(residue)'  

### 4. Get help via the command line via the -h flag  

```
python scripts/calculations.py -h

usage: calculations.py [-h] -i INFILE_CSV -o OUTFILE_CSV -c CONFIG -x
                    OUTFILE_XLSX

Report residue data

optional arguments:
-h, --help            show this help message and exit
-i INFILE_CSV, --infile INFILE_CSV
                        Infile CSV/Excel file for parsing
-o OUTFILE_CSV, --outfile OUTFILE_CSV
                        Outfile CSV for writing
-c CONFIG, --config CONFIG
                        Config file
-x OUTFILE_XLSX, --xlsx OUTFILE_XLSX
                        Outfile XLSX for writing
```

### 5. Run the script on the example data  

```
python scripts/calculations.py -i example_data/NSW_residue_data.csv -o example_data/NSW_residue_data_processed.csv -x example_data/NSW_residue_data_processed.xlsx -c configs/config_nsw.ini
```

Note that if you were using a Windoz machine your paths to the files would be back slashes rather than Unix-like forward slashes.  

If all goes well the following information should be printed to screen based on the example data provided (only first ~20 lines shown)  

```
=====Target=====
Capture year  Region code     Region label  ... Commodity Unit     Estimate
0      2010-11          1.0  New South Wales  ...       NaN  NaN  53566048.12
1      2010-11          1.0  New South Wales  ...       NaN  NaN   1195447.25
2      2010-11          1.0  New South Wales  ...       NaN  NaN   1008032.49
3      2010-11          1.0  New South Wales  ...       NaN  NaN   8817374.49
4      2010-11          1.0  New South Wales  ...       NaN  NaN  42517732.33

[5 rows x 8 columns]
Processing data from australian_capital_territory
scripts/calculations.py:63: UserWarning: Boolean Series key will be reindexed to match DataFrame index.
commod][df['region_label']==region]
Processing data from capital_region
Processing data from central_coast
........
```

## Structure of input files  
The script will automatically lower-case letters and replace spaces with underscores. But if you use uppercase for fields that are also listed in the config file make sure you use the same case in both the input spreadsheet and the config file.  

### Infile CSV/Excel  
The following columns must be included in the infile CSV (comma separated format) or Excel file. Note only the first tab will be processed if this is excel format.  

The best way to get the scripts to work is to follow the style used in the example data for the infile and config files.  

`Capture year` in the format 2010-11, 2011-12, etc. 

`Region code` as a single number ie 1.  

`Region label` for a region, must match label in the comfig file (see below)  

`Commodity description` human friendly label describing the data, information only, not used by the software.  

`Biomass group` ie cropping, fruit, vegetables, etc. Information only, not used by the software.  

`Commodity` type, ie Hay, pasture. This tag must match the entry in the config file so that the correct MC and HI values can be assigned.   

`Unit` For the area under production row this must specifically be labeled as 'Area (ha)' (copy and paste from here). For production rows this value must be either 'Production (kg)' or 'Production (t)'. For yield rows the value must be either 'Yield (t/ha)' or 'Yield (kg/ha)'. Please use this *exact wording format* shown here. The script will convert values to t/ha if provided as Kgs.   

`Estimate` for the Area, Production, and Yield input data for that year, region, and crop. The Area and Production rows will be used to generate the calculations in the final script.  


### Config files  
The config files provide information on the '[RETAIN]', '[HI]' and '[MC]' for regions and different crops.  

**Importantly** only commodities listed in the config file under the '[HI]' tag will be processed, the remainder will be ignored. So please check that Harvest Index values are provided for you target commodities.  

For `[RETAIN]` each row represents data for a region in the format 'lower_case_region_name = 1.0'. The region name must match the `Region label` in the input CSV file. This needs to be in lowercase with underscores representing spaces. If you use double spaces anywhere then you need to specify this with double underscores.   

For the `[HI]` section each line represents a crop type listed in the `Commodity` column of the infile CSV, in lower case, with the harvest index provided as a floating point number ie 'wheat=0.4'. 

`[MC]` tags need to match the field provided in the `Commodity` column in the infile CSV file. This should be a floating point number.

## ToDo  
- currently the year range for the 5 year calcs is recorded on lines 138-139 of the `scripts/calculations.py` script. I need to move this to the config file.  
- check this runs on windozs  

## Update log
- average_cerial_crops = 0.39 has no match in the csv spreadsheet so I deleted it from the `config.ini` file.
- Some of the commods production values are in (t) while others are in kg, these either should all be converted to T or in some way keep track of the unit as this will effect the way the data is displayed.
- also removed `average_oilseed_crops = 0.28` and `average_oilseed_crops = 8.8` for the same reason.  
- averages are based on NSW data and not the indiv regions  
- add yearly data as rows, then a clear row with averages of last years  
- add cols for HI etc  
- convert kg to tonnes  
- handle np values ie act wheat to be 0  
- deal with hay and silage, but dump other hay
- added config parsing and command line help  
- output directly to XLSX and provide a manual calculation tab with hard wired excel like formula for checking calculations manually.  

## Contact  
Any issues contact Dave Wheeler on daveDOTwheelerATdpi.nsw.gov.au. Please replace the DOT with "." and AT with "@". This is to help prevent robots spamming my mail.  
